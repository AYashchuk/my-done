import { shallow, ShallowWrapper } from "enzyme";
import * as React from "react";
import App from "../App";

import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

describe("App component", () => {
  it("Should render normally", () => {
    const appWrapper: ShallowWrapper = shallow(React.createElement(App));
    expect(appWrapper.find(".app").exists()).toEqual(true);
  });
});
