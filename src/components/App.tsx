import React from "react";
import AlertsManager from "../modules/common/alerts";
import ModalManager from "../modules/common/modals/ModalManager/ModalManager";
import Overlay from "../modules/common/overlay";
import Progress from "../modules/common/progress";
import Information from "./Information";

const App = () => (
  <div className="app">
    <Progress />
    <Information />
    <Overlay />
    <ModalManager />
    <AlertsManager />
  </div>
);

export default App;
