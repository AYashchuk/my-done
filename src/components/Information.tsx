import React, { Dispatch } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { showInfoAlert } from "../modules/common/alerts/AlertsManager/store/actions";
import { showTestModal } from "../modules/common/modals/InformationModal";
import { pipe } from "../utiils";

let counter = 0;
const Information = (props: { loadModal: () => void; showAlert }) => (
  <div>
    <button onClick={props.loadModal}>Test modal</button>
    <button onClick={props.showAlert}>Test alert</button>
  </div>
);

const showModalThunk = (dispatch: ThunkDispatch<any, any, any>) => () =>
  pipe({ title: "Information modal" }, showTestModal, dispatch).then(result =>
    console.log(result, "From Information modal")
  );

export default connect(null, dispatch => ({
  loadModal: showModalThunk(dispatch),
  showAlert: () =>
    dispatch(
      showInfoAlert(`Counter number ${counter++}`, "Success Alert is shown")
    )
}))(Information);
