import { LinearProgress } from "@material-ui/core";
import React from "react";
import { connect } from "react-redux";
import { state2Props } from "../store/selectors";

export interface Props {
  isLoading?: boolean;
}

const Progress = ({ isLoading }: Props) =>
  isLoading ? <LinearProgress /> : null;

export default connect<Props, null, {}>(state2Props)(Progress);
