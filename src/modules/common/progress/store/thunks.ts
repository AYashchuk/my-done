import { hideOverlay, showOverlay } from "../../overlay/store/actions";
import { endLoading, startLoading } from "./actions";

export const withProgress = dispatch => (
  promise,
  successHandler,
  errorHandler
) => {
  dispatch(startLoading());
  return promise
    .then(result => {
      dispatch(endLoading());
      return successHandler(result);
    })
    .catch(error => {
      dispatch(endLoading());
      return errorHandler(error);
    });
};

export const withProgressOverlay = dispatch => (
  promise,
  successHandler,
  errorHandler
) => {
  dispatch(startLoading());
  dispatch(showOverlay());
  return promise
    .then(result => {
      dispatch(endLoading());
      return successHandler(result);
    })
    .catch(error => {
      dispatch(endLoading());
      return errorHandler(error);
    })
    .finally(() => dispatch(hideOverlay()));
};
