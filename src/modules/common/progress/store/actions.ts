import { moduleName } from "./selectors";

export const START_LOADING = `${moduleName}/START_LOADING`;
export const END_LOADING = `${moduleName}/END_LOADING`;

export const startLoading = () => ({
  type: START_LOADING
});

export const endLoading = () => ({
  type: END_LOADING
});
