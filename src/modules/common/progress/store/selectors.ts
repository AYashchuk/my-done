import { createSelector } from "reselect";

export const moduleName = "progress";

export const domain = (_: any) => _[moduleName];
export const inProgress = createSelector(domain, _ => _.inProgress);
export const isLoading = createSelector(
  inProgress,
  countOfInProgress => countOfInProgress > 0
);

export const state2Props = (state: any) => ({
  isLoading: isLoading(state)
});
