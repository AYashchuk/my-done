import { Reducer } from "redux";
import { Action } from "../../../redux/models";
import { END_LOADING, START_LOADING } from "./actions";

const initialState = {
  inProgress: 0
};

const reducer: Reducer = (state = initialState, action: Action) => {
  const { type } = action;
  const { inProgress } = state;
  switch (type) {
    case START_LOADING:
      return { inProgress: inProgress + 1 };
    case END_LOADING:
      return { inProgress: inProgress > 0 ? inProgress - 1 : 0 };
    default: {
      return state;
    }
  }
};

export default reducer;
