import { all, takeEvery } from "redux-saga/effects";

function* fetchAllSaga() {
  console.log(1);
}

export default function* saga() {
  yield all([takeEvery("test", fetchAllSaga)]);
}
