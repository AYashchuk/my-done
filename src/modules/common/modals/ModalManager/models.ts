import { Reducer } from "redux";
import { Action, AnyProps } from "../../../redux/models";

export type Show = (modalId: string) => any;

export interface ModalModule {
  show: Show;
  reducer: Reducer;
}

export interface ModalPublicInterface {
  dynamicImport: () => Promise<any>;
  id: string;
}
export interface ModelActionPayload {
  modalId?: string;
  props?: AnyProps;
  resolve?: (result: any) => void;
  outputModalData?: AnyProps;
}
export interface ModalAction extends Action {
  payload?: ModelActionPayload;
}

export interface ResolveMap {
  [key: string]: (result: any) => void;
}
