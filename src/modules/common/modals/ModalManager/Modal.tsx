import Dialog from "@material-ui/core/Dialog";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React from "react";
import { connect } from "react-redux";
import { isModalShowSelector } from "./store/selectors";
import { closeModalThunk } from "./store/thunks";

const Modal = props => {
  const { id, isModalShow, close, ...rest } = props;
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const closeHandler = close(id);
  const onCLoseHandler = () => closeHandler(null);

  return (
    <Dialog
      fullScreen={fullScreen}
      className={id}
      open={isModalShow}
      onClose={onCLoseHandler}
    >
      {React.Children.map(props.children, (child, index) =>
        React.cloneElement(child, {
          bindClose: result => () => closeHandler(result),
          close: closeHandler,
          key: `${id}-${index}`,
          ...rest
        })
      )}
    </Dialog>
  );
};

const mapState2Props = (state, props) => ({
  isModalShow: isModalShowSelector(state, props)
});

const getHandlers = {
  close: closeModalThunk
};

export default connect(mapState2Props, getHandlers)(Modal);
