import { addModalReducer } from "../../../redux/middlewares/addReducer";
import { AnyProps } from "../../../redux/models";
import { retry } from "../../api";
import { withProgressOverlay } from "../../progress/store/thunks";
import { append } from "./ModalManager";
import { ModalAction, ModalModule, ModalPublicInterface } from "./models";
import { modalLoaded } from "./store/actions";

const modals: { [key: string]: (...props: AnyProps[]) => ModalAction } = {};

const getModal = (id: string) => modals[id];
const setModal = (id: string, handler) => (modals[id] = handler);

export const loadModal = (params: ModalPublicInterface) => (
  ...props
) => dispatch => {
  const { id, dynamicImport } = params;

  if (getModal(id)) {
    return dispatch(getModal(id)(...props));
  }

  const successHandler = ({ show, reducer }: ModalModule) => {
    const openModalHandler = show(id);
    setModal(id, openModalHandler);
    append({ dynamicImport, id });
    dispatch(addModalReducer(reducer));
    dispatch(modalLoaded(id));
    return dispatch(openModalHandler(...props));
  };

  return dispatch(withProgressOverlay)(
    retry(dynamicImport()),
    successHandler,
    console.error
  );
};
