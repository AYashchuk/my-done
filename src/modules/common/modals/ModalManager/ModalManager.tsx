import React, { Suspense } from "react";
import { connect } from "react-redux";
import Modal from "./Modal";
import { ModalPublicInterface } from "./models";
import { openedModalsArraySelector } from "./store/selectors";

const modalsMap = {};

export const append = ({ id, dynamicImport }: ModalPublicInterface) =>
  (modalsMap[id] = dynamicImport);
const renderModal = ({ id, ...props }) => {
  const ModalBody = React.lazy(modalsMap[id]);

  return (
    <Modal key={`modal__${id}`} id={id}>
      <ModalBody {...props} />
    </Modal>
  );
};

export const ModalManager = ({ modals }) => (
  <Suspense fallback={null}>{modals.map(renderModal)}</Suspense>
);

export default connect(state => ({
  modals: openedModalsArraySelector(state)
}))(ModalManager);
