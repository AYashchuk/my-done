import { createSelector } from "reselect";

export const moduleName = "modals";

const root = _ => _[moduleName];

const loadedModalsSelector = createSelector(root, _ => _.loadedModals);
const openedModalsPropsSelector = createSelector(
  root,
  _ => _.openedPropsModals
);

export const openedModalsArraySelector = createSelector(
  openedModalsPropsSelector,
  openedModals =>
    Object.keys(openedModals).map(key => ({ ...openedModals[key], id: key }))
);

export const isModalShowSelector = createSelector(
  openedModalsPropsSelector,
  (_, { id }) => id,
  (openedModalsProps, id) => !!openedModalsProps[id]
);
