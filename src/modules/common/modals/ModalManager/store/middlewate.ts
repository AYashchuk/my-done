import { ModalAction, ResolveMap } from "../models";
import { HIDE_MODAL, OPEN_MODAL } from "./actions";

const modalsCloseMap: ResolveMap = {};

export const resolveModalCloseMiddleware = () => next => ({
  type,
  payload
}: ModalAction) => {
  if (type === OPEN_MODAL) {
    const { modalId, resolve } = payload;
    modalsCloseMap[modalId] = resolve;
  }

  if (type === HIDE_MODAL) {
    const { modalId, outputModalData } = payload;
    const resolve = modalsCloseMap[modalId];
    if (resolve) {
      resolve(outputModalData);
    }
  }

  return next({ type, payload });
};
