import { hideModal } from "./actions";

export const closeModalThunk = modalId => dispatch => result =>
  dispatch(hideModal(modalId, result));
