import { AnyProps } from "../../../../redux/models";
import { ModalAction } from "../models";
import { moduleName } from "./selectors";

export const OPEN_MODAL = `${moduleName}/OPEN_MODAL`;
export const HIDE_MODAL = `${moduleName}/HIDE_MODAL`;
export const MODAL_LOADED = `${moduleName}/MODAL_LOADED`;

const openModalAction = (
  modalId: string,
  props: AnyProps,
  resolve
): ModalAction => ({
  payload: {
    modalId,
    props,
    resolve
  },
  type: OPEN_MODAL
});

export const openModal = dispatch => (modalId: string, props: AnyProps) =>
  new Promise(resolve => dispatch(openModalAction(modalId, props, resolve)));

export const modalLoaded = (modalId: string): ModalAction => ({
  payload: {
    modalId
  },
  type: MODAL_LOADED
});

export const hideModal = (
  modalId,
  outputModalData?: AnyProps
): ModalAction => ({
  payload: {
    modalId,
    outputModalData
  },
  type: HIDE_MODAL
});
