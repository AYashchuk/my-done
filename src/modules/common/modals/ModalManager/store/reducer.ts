import { Action, AnyProps } from "../../../../redux/models";
import { createReducerManager } from "../../../../redux/reducerManager";
import { HIDE_MODAL, MODAL_LOADED, OPEN_MODAL } from "./actions";

const loadedModalsInitialState: string[] = [];
const openedModalsPropsInitialState: AnyProps = {};

export const loadedModalsReducer = (
  state = loadedModalsInitialState,
  action
) => {
  const { type, payload } = action;

  if (type === MODAL_LOADED) {
    const { modalId } = payload;
    return !state.includes(modalId) ? [...state, modalId] : state;
  }

  return state;
};

const openedModalsPropsReducer = (
  state = openedModalsPropsInitialState,
  action: Action
) => {
  const { type, payload } = action;
  switch (type) {
    case OPEN_MODAL: {
      const { modalId, props } = payload;
      return {
        ...state,
        [modalId]: props
      };
    }
    case HIDE_MODAL: {
      const { modalId } = payload;
      return {
        ...state,
        [modalId]: null
      };
    }
    default:
      return state;
  }
};

export default createReducerManager({
  loadedModals: loadedModalsReducer,
  openedPropsModals: openedModalsPropsReducer
});
