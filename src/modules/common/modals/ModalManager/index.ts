import ModalManager from "./ModalManager";
import reducer from "./store/reducer";
import { moduleName } from "./store/selectors";

export { reducer, moduleName };
export default ModalManager;
