import { loadModal } from "../ModalManager/loader";
import { ModalPublicInterface } from "../ModalManager/models";
import InformationModal from "./InformationModal";

export const id = "test";

const modal: ModalPublicInterface = {
  dynamicImport: () =>
    import("./public-interface" /* webpackChunkName: "InformationModal" */),
  id
};

export const showTestModal = loadModal(modal);
export default InformationModal;
