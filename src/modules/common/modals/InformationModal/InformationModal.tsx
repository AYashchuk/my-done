import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import React from "react";

let counter = 0;
const InformationModal = props => (
  <React.Fragment>
    <DialogTitle>{props.title || ""}</DialogTitle>
    <DialogContent>
      <DialogContentText>
        This is modal created with the code splitting so, its downloaded and
        showed asynchronously!
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button
        autoFocus={true}
        onClick={props.bindClose({ result: counter++ })}
        color="primary"
      >
        Ok
      </Button>
      <Button onClick={props.close} color="primary">
        Close
      </Button>
    </DialogActions>
  </React.Fragment>
);

export default InformationModal;
