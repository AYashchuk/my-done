import { Show } from "../ModalManager/models";
import InformationModal from "./InformationModal";
import { showTestModalAction } from "./store/actions";
import reducer from "./store/reducer";

const show: Show = (modalId: string) => showTestModalAction(modalId);

export { show, reducer };
export default InformationModal;
