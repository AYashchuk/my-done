import { Show } from "../../ModalManager/models";
import { openModal } from "../../ModalManager/store/actions";

export const showTestModalAction: Show = modalId => props => dispatch =>
  openModal(dispatch)(modalId, props);
