import ModalManager, { moduleName, reducer } from "./ModalManager";

export { moduleName, reducer };
export default ModalManager;
