import AlertsManager from "./AlertsManager";
import reducer from "./AlertsManager/store/reducer";
import saga from "./AlertsManager/store/saga";
import { moduleName } from "./AlertsManager/store/selectors";

export { saga, moduleName, reducer };

export default AlertsManager;
