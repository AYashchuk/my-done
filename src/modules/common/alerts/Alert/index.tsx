import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import { AlertTitle } from "@material-ui/lab";
import MuAlert from "@material-ui/lab/Alert";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { TRANSITION_DURATION } from "../AlertsManager/constants";
import { hideAlert } from "../AlertsManager/store/actions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      "& > * + *": {
        marginTop: theme.spacing(2)
      }
    }
  })
);

const Alert = ({ title, text, hideAlert, id, isOpen, type }) => {
  const classes = useStyles();
  const hideAlertHandler = hideAlert(id);

  return (
    <div className={`${classes.root} alert`}>
      <Collapse timeout={TRANSITION_DURATION} in={isOpen}>
        <MuAlert
          severity={type}
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={hideAlertHandler}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
          {title && <AlertTitle>title</AlertTitle>}
          {text}
        </MuAlert>
      </Collapse>
    </div>
  );
};

const handlers = dispatch => ({
  hideAlert: id => () => dispatch(hideAlert(id, true))
});

export default connect(null, handlers)(Alert);
