export enum TYPES {
  SUCCESS = "success",
  WARNING = "warning",
  ERROR = "error",
  INFO = "info"
}

export const TRANSITION_DURATION = 1000;
export const ALERT_SHOW_DURATION = 3000;
