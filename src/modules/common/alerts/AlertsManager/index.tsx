import React from "react";
import { connect } from "react-redux";
import Alert from "../Alert";
import "./alerts.css";
import { root } from "./store/selectors";

const AlertsManager = ({ alertsQueue }) => (
  <div className="alerts">
    {alertsQueue.map(props => {
      return <Alert key={props.id} {...props} />;
    })}
  </div>
);

const getProps = store => ({
  alertsQueue: root(store)
});

export default connect(getProps)(AlertsManager);
