import { Action } from "../../../../redux/models";
import { TYPES } from "../constants";
import { Alert } from "../models";
import { moduleName } from "./selectors";

export const OPEN_ALERT = `${moduleName}/OPEN_ALERT`;
export const SHOW_ALERT = `${moduleName}/SHOW_ALERT`;
export const HIDE_ALERT = `${moduleName}/HIDE_ALERT`;
export const REMOVE_ALERT = `${moduleName}/REMOVE_ALERT`;
export const REMOVE_ALL_HIDDEN_ALERTS = `${moduleName}/REMOVE_ALL_HIDDEN_ALERTS`;

const showAlert = (alertType: TYPES) => (text, title?): Action => ({
  type: SHOW_ALERT,
  payload: {
    title,
    text,
    isOpen: true,
    type: alertType
  }
});

export const showSuccessAlert = showAlert(TYPES.SUCCESS);
export const showInfoAlert = showAlert(TYPES.INFO);
export const showWarningAlert = showAlert(TYPES.WARNING);
export const showErrorAlert = showAlert(TYPES.ERROR);

export const hideAlert = (id: number, isHideByUser = false): Action => ({
  type: HIDE_ALERT,
  payload: {
    id,
    isHideByUser
  }
});

export const removeAlert = (id: number): Action => ({
  type: REMOVE_ALERT,
  payload: {
    id
  }
});

export const removeAllHiddenAlerts = (alerts: Alert): Action => ({
  type: REMOVE_ALL_HIDDEN_ALERTS,
  payload: {
    alerts
  }
});
