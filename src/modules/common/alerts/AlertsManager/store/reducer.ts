import { Action } from "../../../../redux/models";
import { Alert } from "../models";
import {
  HIDE_ALERT,
  OPEN_ALERT,
  REMOVE_ALERT,
  REMOVE_ALL_HIDDEN_ALERTS
} from "./actions";

export const initialState: Alert[] = [];

export default (state = initialState, action: Action) => {
  const { payload, type } = action;
  switch (type) {
    case OPEN_ALERT: {
      const { id, ...rest } = payload;
      return [
        ...state,
        {
          id,
          ...rest
        }
      ];
    }
    case HIDE_ALERT: {
      const { id, isHideByUser } = payload;
      const index = state.findIndex(item => item.id === id);
      const item = state[index];
      const newState = [...state];
      newState[index] = {
        ...item,
        isHideByUser,
        isOpen: false
      };
      return newState;
    }
    case REMOVE_ALERT: {
      const { id } = payload;
      const index = state.findIndex(item => item.id === id);
      state.splice(index, 1);

      return [...state];
    }

    case REMOVE_ALL_HIDDEN_ALERTS: {
      const { alerts } = payload;
      const ids = alerts.map(({ id }) => id);
      return state.filter(item => !ids.includes(item.id));
    }
    default:
      return state;
  }
};
