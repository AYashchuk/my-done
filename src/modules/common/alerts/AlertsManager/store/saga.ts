import {
  all,
  call,
  delay,
  put,
  select,
  take,
  takeEvery
} from "redux-saga/effects";
import { ALERT_SHOW_DURATION, TRANSITION_DURATION } from "../constants";
import {
  hideAlert,
  OPEN_ALERT,
  removeAlert,
  removeAllHiddenAlerts,
  SHOW_ALERT
} from "./actions";
import {
  getHiddenAlertsByUser,
  getNotHidedByUserAlerts,
  root
} from "./selectors";

const getId = () => new Date().getTime() + Math.random();

function* openAlertSaga(action) {
  try {
    const { payload } = action;
    const id = getId();
    yield put({
      type: OPEN_ALERT,
      payload: {
        ...payload,
        id
      }
    });
  } catch (e) {
    console.error(e);
  }
}

function* removeAllHiddenAlertsByUser() {
  const hiddenAlertsByUser = yield select(getHiddenAlertsByUser);
  if (hiddenAlertsByUser.length) {
    yield put(removeAllHiddenAlerts(hiddenAlertsByUser));
  }
}

function* isQueueNotHiddenAlertsEmpty() {
  const alertsQueue = yield select(getNotHidedByUserAlerts);
  return !!alertsQueue.length;
}

function* hideAlerts() {
  yield call(removeAllHiddenAlertsByUser);
  yield delay(ALERT_SHOW_DURATION);
  const alertsQueue = yield select(getNotHidedByUserAlerts);
  if (!alertsQueue.length) {
    return;
  }
  const { id } = alertsQueue[0];
  yield put(hideAlert(id));
  yield delay(TRANSITION_DURATION);
  yield put(removeAlert(id));
  const isQueueEmpty = yield call(isQueueNotHiddenAlertsEmpty);
  if (isQueueEmpty) {
    yield call(hideAlerts);
  }
}

function* watch() {
  while (true) {
    yield take(SHOW_ALERT);
    const alertsQueue = yield select(root);
    if (alertsQueue.length > 0) {
      return;
    }
    yield call(hideAlerts);
  }
}

export default function* saga() {
  yield all([takeEvery(SHOW_ALERT, openAlertSaga), watch()]);
}
