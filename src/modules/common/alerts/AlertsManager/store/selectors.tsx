import { createSelector } from "reselect";

export const moduleName = "alert";

export const root = _ => _[moduleName];

export const getNotHidedByUserAlerts = createSelector(root, root =>
  root.filter(item => !item.isHideByUser)
);
export const getHiddenAlertsByUser = createSelector(root, root =>
  root.filter(item => item.isHideByUser)
);
