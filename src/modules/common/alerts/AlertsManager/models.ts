import { TYPES } from "./constants";

export interface Alert {
  id: number;
  title: any;
  text: any;
  isOpen: boolean;
  type: TYPES;
  isHideByUser?: boolean;
}
