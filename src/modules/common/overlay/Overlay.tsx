import React from "react";
import { connect } from "react-redux";
import "./overlay.css";
import { getIsOverlayShow } from "./store/selectors";

const Overlay = ({ isShow }) => (isShow ? <div id="overlay" /> : null);

export default connect(state => ({ isShow: getIsOverlayShow(state) }))(Overlay);
