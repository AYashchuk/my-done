import { moduleName } from "./selectors";

export const OVERLAY_SHOW = `${moduleName}/OVERLAY_SHOW`;
export const OVERLAY_HIDE = `${moduleName}/OVERLAY_HIDE`;

export const showOverlay = () => ({
  type: OVERLAY_SHOW
});
export const hideOverlay = () => ({
  type: OVERLAY_HIDE
});
