import { createSelector } from "reselect";

export const moduleName = "overlay";

const root = _ => _[moduleName];

export const getIsOverlayShow = createSelector(root, _ => _.isShow);
