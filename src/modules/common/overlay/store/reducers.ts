import { OVERLAY_HIDE, OVERLAY_SHOW } from "./actions";

const initialState = {
  isShow: false
};

export default (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case OVERLAY_HIDE:
      return initialState;
    case OVERLAY_SHOW:
      return {
        isShow: true
      };
    default:
      return state;
  }
};
