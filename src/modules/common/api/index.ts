const ATTEMPT_COUNT = 5;
const RETRY_TIMEOUT = 1000;

export const retry = (request: Promise<any>, count = ATTEMPT_COUNT) =>
  request
    .then(result => result)
    .catch(error => {
      if (count > 1) {
        return new Promise(resolve =>
          setTimeout(() => resolve(retry(request, count - 1)), RETRY_TIMEOUT)
        );
      }
      throw new Error(error);
    });
