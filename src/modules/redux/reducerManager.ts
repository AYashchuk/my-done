import { Action, combineReducers, Reducer, ReducersMapObject } from "redux";

export interface ReducerManager {
  add: (key: string, reducer: Reducer) => void;
  getReducerMap: () => ReducersMapObject;
  reduce: (state: any, action: Action) => void;
  remove: (key: string) => void;
}

const EMPTY = () => ({});

export function createReducerManager(
  initialReducers?: ReducersMapObject
): ReducerManager {
  let isEmptyReducer = !initialReducers;
  // Create an object which maps keys to reducers
  const reducers = !isEmptyReducer ? { ...initialReducers } : {};
  // Create the initial combinedReducer
  let combinedReducer = !isEmptyReducer ? combineReducers(reducers) : EMPTY;
  // An array which is used to delete state keys when reducers are removed
  let keysToRemove: string[] = [];
  return {
    // Adds a new reducer with the specified key
    add: (key: string, reducer: Reducer) => {
      if (!key || reducers[key]) {
        return;
      }
      // Add the reducer to the reducer mapping
      reducers[key] = reducer;
      // Generate a new combined reducer
      combinedReducer = combineReducers(reducers);
      isEmptyReducer = false;
    },
    getReducerMap: () => reducers,
    // The root reducer function exposed by this object
    // This will be passed to the store
    reduce: (state: any, action: Action) => {
      // If any reducers have been removed, clean up their state first
      if (keysToRemove.length > 0) {
        state = { ...state };
        for (const key of keysToRemove) {
          delete state[key];
        }
        keysToRemove = [];
      }
      // Delegate to the combined reducer
      return combinedReducer(state, action);
    },
    // Removes a reducer with the specified key
    remove: (key: string) => {
      if (!key || !reducers[key]) {
        return;
      }
      // Remove it from the reducer mapping
      delete reducers[key];
      // Add the key to the list of keys to clean up
      keysToRemove.push(key);
      // Generate a new combined reducer
      combinedReducer = combineReducers(reducers);
    }
  };
}
