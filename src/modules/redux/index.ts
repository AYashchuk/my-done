import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import {
  middleware,
  rootReducerManager,
  sagaMiddleware
} from "./applyMiddleware";
import rootSaga from "./middlewares/sgas";

// Create a store with the root reducer function being the one exposed by the manager.
export default createStore(
  rootReducerManager.reduce,
  composeWithDevTools(middleware)
);

sagaMiddleware.run(rootSaga);
