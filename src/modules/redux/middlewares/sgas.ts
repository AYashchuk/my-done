import { all } from "redux-saga/effects";
import { saga as alertSaga } from "../../common/alerts";
import { saga as progressSaga } from "../../common/progress";

export default function*() {
  yield all([progressSaga(), alertSaga()]);
}
