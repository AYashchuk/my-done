import { Middleware } from "redux";
import { ReducerManager } from "../../reducerManager";

const actionPrefix = "add-reducer";

export const ADD_ROOT_REDUCER = `${actionPrefix}/ADD_ROOT_REDUCER`;
export const ADD_MODAL_REDUCER = `${actionPrefix}/ADD_MODAL_REDUCER`;
export const REDUCER_IS_ADDED = `${actionPrefix}/REDUCER_IS_ADDED`;

type AddReducerMiddlewareType = (
  reducerManager: ReducerManager,
  addReducerActionType: string
) => Middleware;

export const addRootReducer = payload => ({
  payload,
  type: ADD_ROOT_REDUCER
});
export const addModalReducer = payload => ({
  payload,
  type: ADD_MODAL_REDUCER
});
export const reducerIsLoaded = payload => ({
  payload,
  type: REDUCER_IS_ADDED
});

export const addReducerMiddleware: AddReducerMiddlewareType = (
  reducerManager: ReducerManager,
  addReducerActionType
) => store => next => action => {
  if (action.type === addReducerActionType) {
    const {
      payload: { reducer, reducerKey }
    } = action;
    reducerManager.add(reducerKey, reducer);
    store.dispatch(reducerIsLoaded(addReducerActionType));
    return;
  }

  return next(action);
};
