import { AnyAction } from "redux";

export interface Action extends AnyAction {
  payload?: AnyProps;
  type: string;
}

export interface AnyProps {
  [id: string]: any;
}
