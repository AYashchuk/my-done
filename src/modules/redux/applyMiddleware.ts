import { applyMiddleware } from "redux";
import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import thunk from "redux-thunk";
import createHistory from "../../setHistory";
import { reducer as modalsReducerManager } from "../common/modals";
import { resolveModalCloseMiddleware } from "../common/modals/ModalManager/store/middlewate";
import configureReducers from "./configureReducers";
import {
  ADD_MODAL_REDUCER,
  ADD_ROOT_REDUCER,
  addReducerMiddleware
} from "./middlewares/addReducer";

export const sagaMiddleware = createSagaMiddleware();

export const rootReducerManager = configureReducers(createHistory);
const addRootReducerMiddleware = addReducerMiddleware(
  rootReducerManager,
  ADD_ROOT_REDUCER
);
const addModalReducerMiddleware = addReducerMiddleware(
  modalsReducerManager,
  ADD_MODAL_REDUCER
);

export const middleware = applyMiddleware(
  thunk,
  logger,
  resolveModalCloseMiddleware,
  addRootReducerMiddleware,
  addModalReducerMiddleware,
  sagaMiddleware
);
