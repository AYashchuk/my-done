import { connectRouter } from "connected-react-router";
import { History } from "history";
import { Reducer, ReducersMapObject } from "redux";
import { reducer as form } from "redux-form";
import {
  moduleName as alertsModuleName,
  reducer as alertsReducer
} from "../common/alerts";
import {
  moduleName as modalsModuleName,
  reducer as modalsReducerManager
} from "../common/modals";
import {
  moduleName as overlayModuleName,
  reducer as overlayReducer
} from "../common/overlay";
import {
  moduleName as progressModuleName,
  reducer as progressReducer
} from "../common/progress";
import { createReducerManager, ReducerManager } from "./reducerManager";

export default (history: History): ReducerManager => {
  const staticReducers: ReducersMapObject = {
    [progressModuleName]: progressReducer,
    [modalsModuleName]: modalsReducerManager.reduce,
    [alertsModuleName]: alertsReducer,
    [overlayModuleName]: overlayReducer,
    form,
    router: connectRouter(history) as Reducer
  };
  return createReducerManager(staticReducers);
};
