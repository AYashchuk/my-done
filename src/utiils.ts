export const pipe = (first, ...rest) =>
  rest.reduce((result, f) => f(result), first);
export const compose = (...params) => input =>
  params.reduce((result, f) => f(result), input);
